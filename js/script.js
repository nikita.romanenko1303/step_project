const tabsTitle = document.querySelectorAll(".tabs-title");
const tabsItem = document.querySelectorAll(".tabs-item");

tabsTitle.forEach(function (item) {
    item.addEventListener("click", function () {
        let currentTabs = item;
        let tabId = currentTabs.getAttribute("data-tab");
        let currentTabID = document.querySelector(tabId);
        tabsTitle.forEach(function (item) {
            item.classList.remove("active");
            item.classList.remove("active_block");
        });

        tabsItem.forEach(function (item) {
            item.classList.remove("active");
            item.classList.remove("figure_flex")
        });
        currentTabs.classList.add("active");
        currentTabs.classList.add("active_block");
        currentTabID.classList.add("active");
        currentTabID.classList.add("figure_flex")
    });
});

const allTabsPhoto = document.querySelectorAll(".our_tabs_photos li");
const ourCategory = document.querySelectorAll(".our_category img");
const loadMore = document.querySelector(".load_more_block");



allTabsPhoto.forEach(function (liTab) {
    liTab.addEventListener("click", function () {
        ourCategory.forEach(function (it){
            it.classList.remove("inactive")
        });
        allTabsPhoto.forEach(function (it) {
            it.classList.remove("active_filter");
        });
        liTab.classList.add("active_filter");
        button();
        ourCategory.forEach(function (it) {
        let currentTab = liTab.getAttribute("data-className");

            if (!it.classList.contains(currentTab)) {
                it.classList.add("inactive");

            }
        });

    });

});


button();
function getInactiveImages() {
    return document.querySelectorAll(".inactive");
}

function showImages(img) {
    img.classList.remove("inactive");
}

function button() {

    if(!allTabsPhoto[0].classList.contains("active_filter")){
        loadMore.classList.remove("load_more_block");
        loadMore.classList.add("inactive");
    } else{
        loadMore.classList.remove("inactive");
        loadMore.classList.add("load_more_block");
    }
    loadMore.addEventListener("click", () => {
        let allImages = getInactiveImages();
        let size = allImages.length > 12 ? 12 : allImages.length;
        for (let i = 0; i < size; i++) {
            showImages(allImages[i]);
        }
        if (allImages.length <= 12){
            loadMore.classList.remove("load_more_block");
            loadMore.classList.add("inactive");
        }

    });
}

let swiper = new Swiper(".mySwiper", {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
});

// function button() {
//     loadMore.addEventListener("click", () => {
//         inactivePhoto.forEach(function (it) {
//             it.classList.remove("inactive");
//
//         });
//         loadMore.classList.remove("load_more_block");
//         loadMore.classList.add("inactive");
//
//     });
// }